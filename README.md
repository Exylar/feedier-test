# Feedier NPS Form Test

Welcome to the Feedier NPS (Net Promoter Score) Form Test! This test project showcases a multi-step NPS form created with Vue.js and styled with TailwindCSS. In addition to the form, this project also displays analytics based on the form responses.

## Features

1. **Multi-step NPS Form**: Built using Vue.js, this form provides a seamless experience for gathering NPS scores.
2. **Analytics Display**: Based on the form submissions, a dynamic analytics section is present which offers insights into the feedback provided.
3. **Responsive Design**: TailwindCSS is utilized to ensure the form and analytics sections are responsive. Test it out on different devices!

## Running the project locally

To get the project up and running on your local machine, follow these steps:

1. **Clone the repository**: 

```git clone https://gitlab.com/Exylar/feedier-test```

2. **Navigate to the project directory**: 

```cd feedier-test```

3. **Install dependencies**: 

```npm install```

4. **Run the local development server**: 

```npm run dev```

Once started, the application will be accessible on `http://localhost:5173` by default.

## Online Version

If you wish to see the project in action without setting it up locally or to check its responsiveness on various devices, it's hosted online at:

[https://feedier-test.web.app/](https://feedier-test.web.app/)

## Feedback & Contributions

Feel free to submit any feedback or suggestions to improve the project. Pull requests are always welcome. Make sure to test any changes thoroughly before submitting a PR.

## License

This project is licensed under the MIT License. See the `LICENSE` file for details.

## Acknowledgements

Big thanks to Feedier for the opportunity to work on this test project. Looking forward to potential collaboration!

---

For more information or any issues, please reach out or open an issue in the repository.
