import { createRouter, createWebHistory } from 'vue-router'

import DashboardLayout from '@/layouts/DashboardLayout/DashboardLayout.vue'
import NoAuthLayout from '@/layouts/NoAuthLayout/NoAuthLayout.vue'

import SurveyView from '@/views/apps/SurveyView.vue'

import DashboardHomeView from '@/views/dashboard/DashboardHomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: NoAuthLayout, 
      redirect: "/",
      children: [
        {
          path: "/",
          name: "Survey",
          component: SurveyView
        },
      ]
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: DashboardLayout,
      redirect: "/dashboard",
      children: [
        {
          path: "/dashboard",
          name: "Home",
          component: DashboardHomeView
        },
      ]
    }
  ]
})

export default router
